package ru.t1.volkova.tm.api.repository;

import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}
