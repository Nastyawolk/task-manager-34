package ru.t1.volkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

public final class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
